fn int_to_hexit(int: u8) -> char {
    if int < 10 {
        return (int + '0' as u8) as char;
    }
    return (int - 10 + 'a' as u8) as char;
}

fn int_to_hex_byte(int: &u8) -> String {
    let upper = (int & 0b11110000) >> 4;
    let lower = int & 0b00001111;
    let mut hex = String::new();
    hex.push(int_to_hexit(upper));
    hex.push(int_to_hexit(lower));
    return hex;
}

fn byte_vec_to_hex_string(bytes: &Vec<u8>) -> String {
    return bytes
        .iter()
        .map(int_to_hex_byte)
        .collect::<String>();
}

fn read_lines(count: u32) -> Vec<String> {
    let mut lines = Vec::new();
    for _ in 0..count {
        let mut line = String::new();
        match std::io::stdin().read_line(&mut line) {
            Ok(_) => {
                if line.len() == 0 {
                    return lines
                }
                lines.push(line.trim().to_string());
            }
            Err(_) => {
                return lines;
            }
        }
    }
    return lines;
}

fn main() {
    let lines = read_lines(std::u32::MAX);
    let mut lineiter = lines.iter();
    let key = lineiter.next().unwrap().as_bytes();
    let mut index = 2;
    let ciphered = lineiter
        .map(|line| line.clone())
        .collect::<Vec<String>>()
        .join("\n")
        .as_bytes()
        .iter()
        .map(|byte| {index = index + 1; byte ^ key[index % key.len()]})
        .collect::<Vec<u8>>();
    println!("{}", byte_vec_to_hex_string(&ciphered));
}
