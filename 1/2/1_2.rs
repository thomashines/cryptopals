fn hexit_to_int(hex: u8) -> u8 {
    if hex < 'a' as u8 {
        return hex - '0' as u8;
    }
    return hex - 'a' as u8 + 10;
}

fn hex_string_to_byte_vec(hex: &str) -> Vec<u8> {
    return hex
        .trim()
        .as_bytes()
        .chunks(2)
        .map(|chunk| {
            16 * hexit_to_int(chunk[0]) + hexit_to_int(chunk[1])
        })
        .collect::<Vec<u8>>();
}

fn int_to_hexit(int: u8) -> char {
    if int < 10 {
        return (int + '0' as u8) as char;
    }
    return (int - 10 + 'a' as u8) as char;
}

fn int_to_hex_byte(int: u8) -> String {
    let upper = (int & 0b11110000) >> 4;
    let lower = int & 0b00001111;
    let mut hex = String::new();
    hex.push(int_to_hexit(upper));
    hex.push(int_to_hexit(lower));
    return hex;
}

fn byte_vec_to_hex_string(bytes: Vec<u8>) -> String {
    return bytes
        .into_iter()
        .map(int_to_hex_byte)
        .collect::<String>();
}

fn read_lines(count: u32) -> Vec<String> {
    let mut lines = Vec::new();
    for _ in 0..count {
        let mut line = String::new();
        match std::io::stdin().read_line(&mut line) {
            Ok(_) => {
                lines.push(line.trim().to_string());
            }
            Err(_) => {
                return lines;
            }
        }
    }
    return lines;
}


fn main() {
    let lines = read_lines(2);
    if lines.len() != 2 {
        println!("failed reading lines");
        return;
    }
    let left = hex_string_to_byte_vec(&lines[0]);
    let right = hex_string_to_byte_vec(&lines[1]);
    let xord = left
        .into_iter()
        .zip(right.into_iter())
        .map(|(ll, rr)| ll ^ rr)
        .collect::<Vec<u8>>();
    let hex = byte_vec_to_hex_string(xord);
    println!("{}", hex);
}
