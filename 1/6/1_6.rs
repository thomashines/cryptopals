fn hammone(left: &u8, right: &u8) -> u32 {
    let mut dist = 0;
    for bit in 0..7 {
        let mask = 1 << bit;
        if left & mask != right & mask {
            dist = dist + 1;
        }
    }
    return dist;
}
fn hammer(left: &[u8], right: &[u8]) -> u32 {
    return left.iter().zip(right.iter())
        .map(|(ll, rr)| hammone(ll, rr))
        .sum();
}

fn read_lines(count: u32) -> Vec<String> {
    let mut lines = Vec::new();
    for _ in 0..count {
        let mut line = String::new();
        match std::io::stdin().read_line(&mut line) {
            Ok(_) => {
                if line.len() == 0 {
                    return lines
                }
                lines.push(line.trim().to_string());
            }
            Err(_) => {
                return lines;
            }
        }
    }
    return lines;
}

fn base64_int_to_digit(int: &u8) -> char {
    if *int < 26 {
        return (*int + ('A' as u8)) as char;
    }
    if *int < 52 {
        return (*int - 26 + ('a' as u8)) as char;
    }
    if *int < 62 {
        return (*int - 52 + ('0' as u8)) as char;
    }
    if *int == 62 {
        return '+';
    }
    return '/';
}

fn base64_digit_to_int(digit: &char) -> u8 {
    if *digit == '+' {
        return 62;
    }
    if *digit == '/' {
        return 64;
    }
    if *digit <= '9' {
        return (*digit as u8) - ('0' as u8) + 52;
    }
    if *digit == '=' {
        return 0;
    }
    if *digit <= 'Z' {
        return (*digit as u8) - ('A' as u8);
    }
    return (*digit as u8) - ('a' as u8) + 26;
}

fn base64_encode(decoded: &Vec<u8>) -> String {
    let mut decoded_clone = decoded.clone();
    decoded_clone.push(0);
    let mut encoded = Vec::new();
    for bit in (0..8 * (decoded_clone.len() - 1)).step_by(6) {
        let (first_mask, first_shift, last_mask, last_shift, right_shift) = match bit % 8 {
            0 => (0b11111100, 0, 0b00000000, 0, 2),
            6 => (0b00000011, 8, 0b11110000, 0, 4),
            4 => (0b00001111, 8, 0b11000000, 0, 6),
            2 => (0b00111111, 0, 0b00000000, 0, 0),
            _ => (0b00000000, 0, 0b00000000, 0, 0),
        };
        let first_value = (if first_mask == 0 { 0 } else { decoded_clone[bit / 8] & first_mask } as u16) << first_shift;
        let last_value = (if last_mask == 0 { 0 } else { decoded_clone[bit / 8 + 1] & last_mask } as u16) << last_shift;
        let value = ((first_value | last_value) >> right_shift) as u8;
        encoded.push(value)
    }
    let mut encoded_string = encoded.iter().map(base64_int_to_digit).collect::<String>();
    while encoded_string.len() % 4 != 0 {
        encoded_string.push('=');
    }
    return encoded_string;
}

fn base64_decode(encoded: &String) -> Vec<u8> {
    let encoded_bytes = encoded.chars().map(|char| base64_digit_to_int(&char)).collect::<Vec<u8>>();
    let mut decoded = Vec::new();
    for bit in (0..6 * encoded_bytes.len()).step_by(8) {
        let (first_mask, first_shift, last_mask, last_shift, right_shift) = match bit % 6 {
            0 => (0b111111, 6, 0b110000, 0, 4),
            2 => (0b001111, 6, 0b111100, 0, 2),
            4 => (0b000011, 6, 0b111111, 0, 0),
            _ => (0b000000, 0, 0b000000, 0, 0),
        };
        let first_value = (if first_mask == 0 { 0 } else { encoded_bytes[bit / 6] & first_mask } as u16) << first_shift;
        let last_value = (if last_mask == 0 { 0 } else { encoded_bytes[bit / 6 + 1] & last_mask } as u16) << last_shift;
        let value = ((first_value | last_value) >> right_shift) as u8;
        decoded.push(value)
    }
    for char in encoded.chars().rev() {
        if char == '=' {
            decoded.pop();
        } else {
            break;
        }
    }
    return decoded;
}

fn etaoin_score(string: &Vec<u8>) -> u32 {
    let reference: &[u8] = b"eEtTaAoOiInN sShHrRdDlLuU aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ,&!.'\"";
    return string
        .iter()
        .map(|&byte| match reference.iter().position(|&char| char == byte) {
            Some(score) => score as u32,
            None => reference.len() as u32 + 1
        })
        .sum();
}

fn main() {
    // This prints 37
    // println!("{}",
    //     hammer("this is a test".as_bytes(),
    //         "wokka wokka!!!".as_bytes()));

    let lines = read_lines(std::u32::MAX);
    let encoded = lines
        .iter()
        .map(String::clone)
        .collect::<String>();
    let decoded = base64_decode(&encoded);

    let mut scores = Vec::new();

    // for keysize in 1..decoded.len() {
    for keysize in 2..40 {
        let mut total_dist = 0;
        let mut total_pairs = 0;

        for first_index in (0..decoded.len() - keysize).step_by(keysize) {
            for second_index in (first_index + keysize..decoded.len() - keysize).step_by(keysize) {
                total_dist = total_dist + hammer(&decoded[first_index..first_index + keysize],
                    &decoded[second_index..second_index + keysize]);
                total_pairs = total_pairs + 1;
            }
        }

        if total_pairs > 0 {
            scores.push((total_dist as f64 / ((total_pairs * keysize) as f64), keysize));
        }
    }

    scores.sort_by(|a, b| a.partial_cmp(b).unwrap());

    for (_score, keysize) in scores {
        println!("keysize {}", keysize);

        let mut transposed = Vec::new();
        for byte in 0..keysize {
            let mut bytes = Vec::new();
            for index in (byte..decoded.len() - 1).step_by(keysize) {
                bytes.push(decoded[index]);
            }
            transposed.push(bytes);
        }

        let mut key = Vec::new();
        for bytes in transposed {
            let mut best_cipher = 0;
            let mut best_score = std::u32::MAX;
            for cipher in 0..std::u8::MAX {
                let deciphered = bytes.iter().map(|byte| byte ^ cipher).collect::<Vec<u8>>();
                let score = etaoin_score(&deciphered);
                if score < best_score {
                    best_cipher = cipher;
                    best_score = score;
                }
            }
            key.push(best_cipher);
        }

        let mut index = keysize - 1;
        let deciphered = decoded
            .iter()
            .map(|byte| {
                index = index + 1;
                std::cmp::max(32, std::cmp::min(122, byte ^ key[index % key.len()])) as char
            })
            .collect::<String>();

        println!("deciphered {}", deciphered);

        // Optionally,
        break;
    }

}
