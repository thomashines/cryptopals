fn hexit_to_int(hex: u8) -> u8 {
    if hex < 'a' as u8 {
        return hex - '0' as u8;
    }
    return hex - 'a' as u8 + 10;
}

fn hex_string_to_byte_vec(hex: &str) -> Vec<u8> {
    return hex
        .trim()
        .as_bytes()
        .chunks(2)
        .map(|chunk| {
            16 * hexit_to_int(chunk[0]) + hexit_to_int(chunk[1])
        })
        .collect::<Vec<u8>>();
}

fn bytes_to_string(bytes: &Vec<u8>) -> String {
    return bytes
        .into_iter()
        .map(|byte| *byte as char)
        .collect::<String>();
}

fn read_lines(count: u32) -> Vec<String> {
    let mut lines = Vec::new();
    for _ in 0..count {
        let mut line = String::new();
        match std::io::stdin().read_line(&mut line) {
            Ok(_) => {
                lines.push(line.trim().to_string());
            }
            Err(_) => {
                return lines;
            }
        }
    }
    return lines;
}

fn etaoin_score(string: &Vec<u8>) -> u32 {
    let reference: &[u8] = b"eEtTaAoOiInN sShHrRdDlLuU";
    return string
        .clone()
        .into_iter()
        .map(|byte| match reference.iter().position(|&ch| ch == byte) {
            Some(score) => score as u32,
            None => reference.len() as u32 + 1
        })
        .sum()
}


fn main() {
    let lines = read_lines(1);
    if lines.len() != 1 {
        println!("failed reading lines");
        return;
    }
    let input = hex_string_to_byte_vec(&lines[0]);
    let mut best_score = std::u32::MAX;
    for cipher in 0..255 {
        let deciphered = input
            .clone()
            .into_iter()
            .map(|byte| byte ^ cipher)
            .collect::<Vec<u8>>();
        let score = etaoin_score(&deciphered);
        if score < best_score {
            best_score = score;
            println!("{} {}", best_score, bytes_to_string(&deciphered));
        }
    }
}
