fn hex_byte_to_int(hex: u8) -> u8 {
    if hex < ('a' as u8) {
        return hex - ('0' as u8);
    }
    return hex - ('a' as u8) + 10;
}

fn int_to_hexit(int: u8) -> char {
    if int < 26 {
        return (int + ('A' as u8)) as char;
    }
    if int < 52 {
        return (int - 26 + ('a' as u8)) as char;
    }
    if int < 62 {
        return (int - 52 + ('0' as u8)) as char;
    }
    if int == 62 {
        return '+';
    }
    return '/';
}

fn main() {
    let mut input = String::new();
    match std::io::stdin().read_line(&mut input) {
        Ok(_) => {
            let mut input_bytes = input
                .trim()
                .as_bytes()
                .chunks(2)
                .map(|chunk| {
                    16 * hex_byte_to_int(chunk[0]) + hex_byte_to_int(chunk[1])
                })
                .collect::<Vec<u8>>();
            input_bytes.push(0);
            let mut encoded = Vec::new();
            for bit in (0..8 * (input_bytes.len() - 1)).step_by(6) {
                let (first_mask, first_shift, last_mask, last_shift, right_shift) = match bit % 8 {
                    0 => (0b11111100, 0, 0b00000000, 0, 2),
                    6 => (0b00000011, 8, 0b11110000, 0, 4),
                    4 => (0b00001111, 8, 0b11000000, 0, 6),
                    2 => (0b00111111, 0, 0b00000000, 0, 0),
                    _ => (0b00000000, 0, 0b00000000, 0, 0),
                };
                let first_value = (if first_mask == 0 { 0 } else { input_bytes[bit / 8] & first_mask } as u16) << first_shift;
                let last_value = (if last_mask == 0 { 0 } else { input_bytes[bit / 8 + 1] & last_mask } as u16) << last_shift;
                let value = ((first_value | last_value) >> right_shift) as u8;
                encoded.push(int_to_hexit(value))
            }
            match input_bytes.len() % 3 {
                0 => {
                    encoded.push('=');
                },
                1 => {
                },
                2 => {
                    encoded.push('=');
                    encoded.push('=');
                },
                _ => {},
            }
            println!("{}", encoded.into_iter().collect::<String>());
        }
        Err(error) => println!("error: {}", error),
    }
}
